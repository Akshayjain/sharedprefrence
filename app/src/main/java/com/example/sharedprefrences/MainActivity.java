package com.example.sharedprefrences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    EditText editText;
    Button applyTextButton;
    Button savebutton;
    Switch switchbutton;
    public static final String SHAREDPREF = "sharedprefs";
    public static final String TEXT = "text";
    public static final String SWITCH1 = "switch1";
    String getText;
    boolean switchonoff;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textview);
        editText = findViewById(R.id.edittext);
        applyTextButton = findViewById(R.id.apply_text_button);
        savebutton = findViewById(R.id.save_button);
        switchbutton = findViewById(R.id.switch1);

        applyTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView.setText(editText.getText().toString());
            }
        });
        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedata();
            }
        });
        loadData();
        updateviews();
    }

//    saving the data in sharedPrefre
    private void savedata() {

        SharedPreferences sharedPreferences = getSharedPreferences(SHAREDPREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEXT, textView.getText().toString());
        editor.putBoolean(SWITCH1, switchbutton.isChecked());
        editor.apply();
        Toast.makeText(this, "data saved", Toast.LENGTH_SHORT).show();

    }

//   by this method data will be loaded on the screen .. the data that was saved last time
    public void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHAREDPREF, MODE_PRIVATE);
        getText = sharedPreferences.getString(TEXT, "nhi mila");
        switchonoff = sharedPreferences.getBoolean(SWITCH1, false);
    }
//   after saving the data this is how views will be updated
    public void updateviews() {

        textView.setText(getText);
        switchbutton.setChecked(switchonoff);
    }
}
